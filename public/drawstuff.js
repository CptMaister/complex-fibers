/// 2023 by Alex 'Taulex' Mai aka CptMaister
// this script needs the math.js library to work, there is no fallback


const TRANSFORMER_FUNCTIONS = {
  INVERSE_SQUARE: {
    func: z => math.nthRoots(z, 2), // this function should return a complex number or an array of complex numbers
    formula: "\\( \\sqrt{y} \\)",
    description: "Inverse of \\( z \\mapsto z^2 \\), the complex squaring map.",
    expectedNumberOfResults: 2, // this is useful for when your function usually always has more than one result, but for some values the results are the same and the duplicate values are ignored. Like when calling math.nthRoots(x, 3) on a non-zero x gives 3 results, but for x = 0 it is only one result. If the results array is too small, the last value will be appended enough times. This only properly works as intended if the last value is the duplicate one.
  },
  INVERSE_SQUARE_NO_SCALING: {
    func: z => math.nthRoots(z, 2).map(result => math.multiply(result, math.abs(result))),
    formula: "\\( \\sqrt{y} \\cdot | \\sqrt{y} | \\)",
    description: "Inverse of \\( z \\mapsto z^2 \\), but without scaling the absolute value.",
    expectedNumberOfResults: 2,
  },
  INVERSE_CUBE: {
    func: z => math.nthRoots(z, 3),
    formula: "\\( \\sqrt[3]{y} \\)",
    description: "Inverse of \\( z \\mapsto z^3 \\), the complex cubing map.",
    expectedNumberOfResults: 3,
  },
  INVERSE_CUBE_NO_SCALING: {
    func: z => math.nthRoots(z, 3).map(result => math.multiply(result, math.abs(math.pow(result, 2)))),
    formula: "\\( \\sqrt[3]{y} \\cdot | \\sqrt[3]{y} |^2 \\)",
    description: "Inverse of \\( z \\mapsto z^3 \\), but without scaling the absolute value.",
    expectedNumberOfResults: 3,
  },
  INVERSE_BIRKHOFF: {
    func: z => {
      const tmpResult = math.sqrt(math.subtract(math.pow(z, 2), 1));
      return [
        math.add(z, tmpResult),
        math.subtract(z, tmpResult)
      ]
    },
    formula: "\\( y \\mapsto y \\pm \\sqrt{y^2 - 1} \\)",
    description: "Inverse of \\( z \\mapsto \\frac{1}{2} (z + \\frac{1}{z}) \\), the Birkhoff Regularization.",
  },
  INVERSE_BIRKHOFF_ADJUSTED: {
    func: z => {
      const tmpResult = math.sqrt(math.subtract(math.pow(z, 2), 1));
      return [
        ((z.re > 0) || (z.re == 0 && z.im > 0) ? math.add : math.subtract)(z, tmpResult),
        ((z.re > 0) || (z.re == 0 && z.im > 0) ? math.subtract : math.add)(z, tmpResult)
      ]
    },
    formula: "\\( y \\mapsto y \\pm \\sqrt{y^2 - 1} \\)",
    description: "Inverse of \\( z \\mapsto \\frac{1}{2} (z + \\frac{1}{z}) \\), the Birkhoff Regularization, adjusted order of sqrt-results.",
  },
}
const DRAWING_MODE = {
  DOTS: "dots",
  DOTS_TRANSFORMED_ONLY: "dots-transformed-only",
  CONNECTED_NAIVE: "connected-naive",
  CONNECTED_CLOSEST: "connected-closest",
  CONNECTED_CLOSEST_WITH_COLORS: "connected-closest-with-colors",
}



const settings = {
}

const mathSettings = {
  transformerFunctions: [
    {
      transformer: TRANSFORMER_FUNCTIONS.INVERSE_BIRKHOFF_ADJUSTED,
      lineColors: [
        // standard, with unit circle color shift for birkhoff and third color for cube inverse:
        ["#0000ff", "#ff00ff"], // blue, pink
        ["#00aa00", "#ffa500"], // dark green, yellow
        ["#ff8500", "#00aa00"], // TODO lol

        // only black:
        // "#000000", // black

        // ["#0000ff", "#ff00ff"],
        // ["#ff00ff", "#0000ff"],

      ],
      lineWidths: [5],
    },
  ],
}

const drawLogicSettings = {
  minDistanceForDrawing: 0.025,
  maxDistanceForDrawing: 0.01,
  perturbZeroesOnOriginalCanvas: true,
  perturbZeroesOnOriginalCanvasEpsilon: 0.01,
  perturbZeroesOnTransformedCanvasEpsilon: 0.000001,

  // drawingMode: DRAWING_MODE.CONNECTED_CLOSEST,

  drawingMode: DRAWING_MODE.CONNECTED_NAIVE,
  birkhoffNoUnitCircleJumps: true,
  birkhoffStyleSwitch: true,
}

const styleSettings = {
  drawWidth: 5,
  drawColor: ["#ff0000", "#0000a0"], // standard
  // drawColor: ["#000000"], // black

  // coordinateSystemLinesColorAxes: "#000000b0",
  coordinateSystemLinesColorAxes: "#202020",
  coordinateSystemLinesColorUnitCircle: "#000000b0",
  // coordinateSystemLinesColorUnits: "#00000070",
  coordinateSystemLinesColorUnits: "#404040",
  // coordinateSystemLinesColorFifths: "#00000020",
  coordinateSystemLinesColorFifths: "#d0d0d0",
  // coordinateSystemLinesColorTenths: "#00000020",
  coordinateSystemLinesColorTenths: "#d0d0d0",
  coordinateSystemLinesColorHundreths: "#bbbbbb",
  
  coordinateSystemLineWidthAxes: 4,
  coordinateSystemLineWidthUnitCircle: 3,
  coordinateSystemLineWidthUnits: 1.5,
  coordinateSystemLineWidthFifths: 1.2,
  coordinateSystemLineWidthTenths: 1,
  coordinateSystemLineWidthHundreths: 0,

  coordinateSystemLineLengthScaleFactor: 1,

  coordinateSystemPixelsPerUnit: 100,
  transformedCoordinateSystemPixelsPerUnit: 100,

  coordinateSystemTransformable: false,
  coordinateSystemLinesMaxDistanceForDrawing: 0.1,

  backgroundColor: "white",
  textColor: "black",
}


/// one time only setup stuff

// html elements
const divUi = document.getElementById("divUi");
const overlayCenter = document.getElementById("overlayCenter");

const buttonSettingsMenu = document.getElementById("buttonSettingsMenu");
const spanStartInfo = document.getElementById("spanStartInfo");
const buttonResetDrawing = document.getElementById("buttonResetDrawing");
const buttonChangeStuff = document.getElementById("buttonChangeStuff");
const buttonCoordinateSystemTransformation = document.getElementById("buttonCoordinateSystemTransformation");
const buttonVisibilityToggle = document.getElementById("buttonVisibilityToggle");

const body = document.getElementById("body");
body.onkeydown = handleKeyDown;

const papersContainer = document.getElementById("papers-container");
const papers = [
  {
    canvas: document.getElementById("paper1"),
    pen: document.getElementById("paper1").getContext("2d"), // see https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
    unitLength: styleSettings.coordinateSystemPixelsPerUnit,
  },
  {
    canvas: document.getElementById("paper2"),
    pen: document.getElementById("paper2").getContext("2d"),
    unitLength: styleSettings.transformedCoordinateSystemPixelsPerUnit,
  },
];
const transformerHint = document.getElementById("transformer-hint");
updateTransformerHint();

function updateTransformerHint() {
  transformerHint.innerHTML = mathSettings.transformerFunctions[0].transformer.formula + " /// " + mathSettings.transformerFunctions[0].transformer.description;
  try {
    MathJax.typeset();
  } catch (e) {
    console.warn("MathJax not loaded yet (normal for the initial first load).");
  }
  
}

let currentTodoDeleteThisLaterOn = 0;
function toggleTransformer() {
  console.log("TODO DELETE")
  currentTodoDeleteThisLaterOn = (currentTodoDeleteThisLaterOn + 1) % 3;
  if (currentTodoDeleteThisLaterOn == 0) {
    mathSettings.transformerFunctions[0].transformer = TRANSFORMER_FUNCTIONS.INVERSE_BIRKHOFF_ADJUSTED
    drawLogicSettings.drawingMode = DRAWING_MODE.CONNECTED_NAIVE;
    drawLogicSettings.birkhoffNoUnitCircleJumps = true;
    drawLogicSettings.birkhoffStyleSwitch = true;
  } else if (currentTodoDeleteThisLaterOn == 1) {
    mathSettings.transformerFunctions[0].transformer = TRANSFORMER_FUNCTIONS.INVERSE_SQUARE
    drawLogicSettings.drawingMode = DRAWING_MODE.CONNECTED_CLOSEST_WITH_COLORS;
    drawLogicSettings.birkhoffNoUnitCircleJumps = false;
    drawLogicSettings.birkhoffStyleSwitch = false;
  } else {
    mathSettings.transformerFunctions[0].transformer = TRANSFORMER_FUNCTIONS.INVERSE_CUBE_NO_SCALING
    drawLogicSettings.drawingMode = DRAWING_MODE.CONNECTED_CLOSEST_WITH_COLORS;
    drawLogicSettings.birkhoffNoUnitCircleJumps = false;
    drawLogicSettings.birkhoffStyleSwitch = false;
  }
  updateTransformerHint();
  resetCanvases();
}
buttonChangeStuff.addEventListener('click', toggleTransformer);


function toggleCoordinateSystemTransformation() {
  console.log("TODO replace by settings")
  styleSettings.coordinateSystemTransformable = !styleSettings.coordinateSystemTransformable;
  resetCanvases();
}
buttonCoordinateSystemTransformation.addEventListener('click', toggleCoordinateSystemTransformation)


// check settings
papers.map((paper, index) => {
  if (paper.canvas == null) {
    console.warn(`settings canvas element not found for index ${index}; check the id string for typos.`);
  }
  if (paper.pen == null) {
    console.warn(`settings pen element not found for index ${index}; probably the canvas is either not found or not actually a canvas.`);
  }
  if (paper.unitLength <= 1) {
    console.warn(`settings unitLength too small for index ${index}; consider a value bigger than 1.`);
  }
})



// some ui stuff
divUi.onclick = hideInfo;
overlayCenter.onclick = hideInfo;
function hideInfo() {
  if (spanStartInfo.style.display != "none") {
    spanStartInfo.style.display = "none"
  }
}

buttonVisibilityToggle.onclick = toggleUiVisibility;
let uiVisibile = true;
divUi.style.opacity = uiVisibile ? 1 : 0;
function toggleUiVisibility() {
  uiVisibile = !uiVisibile
  divUi.style.opacity = uiVisibile ? 1 : 0;
}

buttonResetDrawing.onclick = () => resetCanvases(false);
function resetCanvases(onlyIfNothingDrawn) {
  if (onlyIfNothingDrawn && hasDrawnSomething) {
    return;
  }

  updateDimensions();
  drawBackground();

  hasDrawnSomething = false;
  papersContainer.style.resize = "both";
  
  cumulativeIndexShifts = 0;
  cumulativeStyleShiftInside = 0;
}


/// handle keyboard shortcuts
function handleKeyDown(event) {
  switch (event.key.toLowerCase()) {
    case ("v"):
      toggleUiVisibility();
      return;
    case ("r"):
      resetCanvases();
      return;
    case ("c"):
      toggleTransformer();
      return;
    default:
      return;
  }
}





/// update the global variables
const LAYOUT = { RESPONSIVE: "responsive", VERTICAL: "vertical", HORIZONTAL: "horizontal" }
let papersLayout = LAYOUT.RESPONSIVE
function updateDimensions() {
  const widthToHeightRatio = papersContainer.clientWidth / papersContainer.clientHeight;
  if (
    papersLayout == LAYOUT.HORIZONTAL ||
    (papersLayout == LAYOUT.RESPONSIVE && widthToHeightRatio >= 1)
  ) {
    papersContainer.style.flexDirection = "row";
    papers.map(paper => {
      paper.canvas.style.width = "50%";
      paper.canvas.style.height = "initial";
    });
  } else if (
    papersLayout == LAYOUT.VERTICAL ||
    (papersLayout == LAYOUT.RESPONSIVE && widthToHeightRatio < 1)
  ) {
    papersContainer.style.flexDirection = "column-reverse";
    papers.map(paper => {
      paper.canvas.style.width = "initial";
      paper.canvas.style.height = "50%";
    });
  }

  papers.map(paper => {
    const canvas = paper.canvas;

    canvas.width = canvas.clientWidth;
    canvas.height = canvas.clientHeight;
    paper.center = {
      x: canvas.width / 2,
      y: canvas.height / 2
    }
  });
}

function drawBackground(bgPapers, trFunctions) {
  const backgroundPapers = bgPapers ?? papers;
  const transformerFunctions = (trFunctions && trFunctions.length === backgroundPapers.length) ? trFunctions : [null, mathSettings?.transformerFunctions[0]?.transformer.func ?? null];

  backgroundPapers.map((paper, index) => {
    paper.pen.clearRect(0, 0, paper.canvas.width, paper.canvas.height); // clear the canvas. Previously the canvas was cleared by resetting paper.width, width "paper.width = paper.clientWidth", which is not very performant, see https://stackoverflow.com/a/6722031
    drawCoordinateSystem(index, transformerFunctions[index]);
  });
}

function drawCoordinateSystem(paperIndex, trFunction) {

  const paper = papers[paperIndex]

  const transformerFunction = styleSettings.coordinateSystemTransformable ? (trFunction ?? null) : null;
  const scaleLinesFactor = transformerFunction === null ? 1 : styleSettings.coordinateSystemLineLengthScaleFactor;

  // this method assumes that the center is always visible within the canvas, not outside somewhere
  const pen = paper.pen;
  const unitLength = paper.unitLength;

  pen.lineCap = "square"; // butt (default), round, square

  // repeating lines
  const heightUp = clamp(paper.center.y, 0, paper.canvas.height) * scaleLinesFactor;
  const heightDown = clamp(paper.canvas.height - paper.center.y, 0, paper.canvas.height) * scaleLinesFactor;
  const widthLeft = clamp(paper.center.x, 0, paper.canvas.width) * scaleLinesFactor;
  const widthRight = clamp(paper.canvas.width - paper.center.x, 0, paper.canvas.width) * scaleLinesFactor;
  [
    {
      systemLineWidth: styleSettings.coordinateSystemLineWidthHundreths,
      systemLineColor: styleSettings.coordinateSystemLinesColorHundreths,
      factor: 1/100
    },
    {
      systemLineWidth: styleSettings.coordinateSystemLineWidthTenths,
      systemLineColor: styleSettings.coordinateSystemLinesColorTenths,
      factor: 1/10
    },
    {
      systemLineWidth: styleSettings.coordinateSystemLineWidthFifths,
      systemLineColor: styleSettings.coordinateSystemLinesColorFifths,
      factor: 1/5
    },
    {
      systemLineWidth: styleSettings.coordinateSystemLineWidthUnits,
      systemLineColor: styleSettings.coordinateSystemLinesColorUnits,
      factor: 1
    },
    {
      systemLineWidth: styleSettings.coordinateSystemLineWidthAxes,
      systemLineColor: styleSettings.coordinateSystemLinesColorAxes,
      factor: 0
    },
  ].map(entry => {
    if (entry.systemLineWidth > 0) {
      pen.lineWidth = entry.systemLineWidth;
      pen.strokeStyle = entry.systemLineColor;

      lines = []
      if (entry.factor === 0) {
        addHorizontalLine(lines, paper, paper.center.y, scaleLinesFactor);
        addVerticalLine(lines, paper, paper.center.x, scaleLinesFactor);
      } else {
        const stepSize = unitLength * entry.factor;
        // horizontal lines above x-axis
        range(stepSize, heightUp, stepSize).map(height => {
          addHorizontalLine(lines, paper, paper.center.y - height, scaleLinesFactor);
        });
        // horizontal lines below x-axis
        range(stepSize, heightDown, stepSize).map(height => {
          addHorizontalLine(lines, paper, paper.center.y + height, scaleLinesFactor);
        });
        // vertical lines left of y-axis
        range(stepSize, widthLeft, stepSize).map(width => {
          addVerticalLine(lines, paper, paper.center.x - width, scaleLinesFactor);
        });
        // vertical lines right of y-axis
        range(stepSize, widthRight, stepSize).map(width => {
          addVerticalLine(lines, paper, paper.center.x + width, scaleLinesFactor);
        });
      }


      if (transformerFunction === null) {
        lines.map(line => drawLineWithFromTo(pen, line));
      } else {
        console.log("lines: ", lines.length);
        lines.map(line => {
          let complexLineTo = getComplexNumberFromPaper(paper, line.to.x, line.to.y);
          let complexLineFrom = perturbZeroes(
            getComplexNumberFromPaper(paper, line.from.x, line.from.y),
            complexLineTo,
            drawLogicSettings.perturbZeroesOnTransformedCanvasEpsilon
          );

          const maxDistance = styleSettings.coordinateSystemLinesMaxDistanceForDrawing;
          const positions = (
            (maxDistance == undefined || maxDistance <= 0) ?
            [complexLineFrom, complexLineTo] :
            spread(complexLineFrom, complexLineTo, maxDistance)
          );

          let transformedFroms = transformerFunction(positions[0]);
          for (let i = 1; i < positions.length; i++) {
            const transformedTos = transformerFunction(positions[i]);

            transformedFroms.map((cFrom, tIndex) => {
              const cTo = transformedTos[tIndex];
              paintLineBetweenComplexNumbersOnPaper(cFrom, cTo, paperIndex);
            });

            transformedFroms = transformedTos;
          }

        })
      }
    }
  })

  // // unit circle
  // pen.strokeStyle = styleSettings.coordinateSystemLinesColorUnitCircle;
  // pen.lineWidth = styleSettings.coordinateSystemLineWidthUnitCircle;
  // drawArc(pen, paper.center, unitLength, 0, Math.PI * 2, false)

  // // axes
  // pen.strokeStyle = styleSettings.coordinateSystemLinesColorAxes;
  // pen.lineWidth = styleSettings.coordinateSystemLineWidthAxes;
  // drawLineHorizontal(pen, paper.center.y) // x axis
  // drawLineVertical(pen, paper.center.x) // y axis
}

function addHorizontalLine(linesArray, paper, yCoord, scaleLinesFactor) {
  linesArray.push({
    from: {x: paper.center.x, y: yCoord},
    to: {x: paper.center.x - scaleLinesFactor * paper.center.x, y: yCoord}
  });
  linesArray.push({
    from: {x: paper.center.x, y: yCoord},
    to: {x: paper.center.x + scaleLinesFactor * (paper.canvas.width - paper.center.x), y: yCoord}
  });
}
function addVerticalLine(linesArray, paper, xCoord, scaleLinesFactor) {
  linesArray.push({
    from: {x: xCoord, y: paper.center.y},
    to: {x: xCoord, y: paper.center.y - scaleLinesFactor * paper.center.y}
  });
  linesArray.push({
    from: {x: xCoord, y: paper.center.y},
    to: {x: xCoord, y: paper.center.y + scaleLinesFactor * (paper.canvas.height - paper.center.y)}
  });
}

function resize() {
  resetCanvases(true);
}


// drawing methods, some stuff copied from https://www.geeksforgeeks.org/how-to-draw-with-mouse-in-html-5-canvas/
let isPainting = false;
let hasDrawnSomething = false;
let lastPosition = undefined;
let lastTransformedEntries = undefined;
let resizedPapersContainer = false;

function handleCanvasMouseDown(event) {
  startPainting(event);
}
function startPainting(event) {
  togglePainting(true, event);
}

function handleMouseUp() {
  if (resizedPapersContainer === true) {
    resizedPapersContainer = false;
    resetCanvases(true);
  }
  stopPainting();
}
function stopPainting() {
  togglePainting(false);
}

function togglePainting(paintBool, event) {
  isPainting = paintBool;
  if (paintBool) {
    hideInfo();

    lastPosition = getComplexNumberForPaper(event, 0);
    handleDrawOriginal(lastPosition, lastPosition, 0);
    
    if (!hasDrawnSomething) {
      hasDrawnSomething = true;
      papersContainer.style.resize = "none";
    }

    lastTransformedEntries = handleTransformerFunctionsOfComplexNumber(lastPosition, mathSettings.transformerFunctions);
    handleDrawTransformedEntries({
      lastTransformedEntries,
      newTransformedEntries: lastTransformedEntries,
      paperIndex: 1
    });
  } else {
    lastPosition = undefined;
  }
}

function paint(event) {
  if (!isPainting) {
    return;
  }

  if (lastPosition == undefined || lastTransformedEntries == undefined) {
    console.error("ERROR: oh man, something went wrong when painting was toggled, lastPosition or lastTransformedEntries is undefined!")
  } else {
    const newPosition = getComplexNumberForPaper(
      event, 0,
      (drawLogicSettings.perturbZeroesOnOriginalCanvas ? lastPosition : undefined)
    );
    if (
      drawLogicSettings.minDistanceForDrawing != undefined &&
      distanceComplex(lastPosition, newPosition) < drawLogicSettings.minDistanceForDrawing
    ) {
      return;
    }

    const positions = (
      (drawLogicSettings.maxDistanceForDrawing == undefined || drawLogicSettings.maxDistanceForDrawing <= 0) ?
      [lastPosition, newPosition] :
      spread(
        lastPosition,
        newPosition,
        drawLogicSettings.maxDistanceForDrawing,
        (drawLogicSettings.perturbZeroesOnOriginalCanvas ? drawLogicSettings.perturbZeroesOnOriginalCanvasEpsilon : 0)
      )
    );
    for (let i = 0; i < positions.length - 1; i++) {
      const fromPosition = positions[i];
      const toPosition = positions[i + 1];

      handleDrawOriginal(
        (drawLogicSettings.drawingMode == DRAWING_MODE.DOTS ? toPosition : fromPosition),
        toPosition,
        0
      );

      let interrupted;
      if (drawLogicSettings.birkhoffNoUnitCircleJumps) {
        interrupted = checkBirkhoffCrossing(fromPosition, toPosition);
      }

      const toTransformedEntries = handleTransformerFunctionsOfComplexNumber(toPosition, mathSettings.transformerFunctions);

      switch (drawLogicSettings.drawingMode) {
        case DRAWING_MODE.DOTS:
        case DRAWING_MODE.DOTS_TRANSFORMED_ONLY:
          handleDrawTransformedEntries({
            lastTransformedEntries,
            newTransformedEntries: toTransformedEntries,
            paperIndex: 1,
            interrupted: true,
          }); // DOTS
          break;
        case DRAWING_MODE.CONNECTED_NAIVE:
          handleDrawTransformedEntries({
            lastTransformedEntries: lastTransformedEntries,
            newTransformedEntries: toTransformedEntries,
            paperIndex: 1,
            interrupted,
          }); // CONNECTED_NAIVE
          break;
        case DRAWING_MODE.CONNECTED_CLOSEST:
        case DRAWING_MODE.CONNECTED_CLOSEST_WITH_COLORS:
          handleDrawTransformedEntriesCorrectedEntryOrder(lastTransformedEntries, toTransformedEntries, 1); // CONNECTED_CLOSEST
          break;
        default:
          handleDrawTransformedEntries({
            lastTransformedEntries,
            newTransformedEntries: toTransformedEntries,
            paperIndex: 1,
            interrupted: true,
          }); // DOTS
          break;
      }

      lastPosition = toPosition;
      lastTransformedEntries = toTransformedEntries;
    }
  }
}

function getComplexNumberForPaper(event, paperIndex, lastNumber) {
  const paper = papers[paperIndex];
  
  const xInCanvas = event.pageX - paper.canvas.offsetLeft;
  const yInCanvas = event.pageY - paper.canvas.offsetTop; // this looks weird because the y-coordinates in a browser are positive downwards
  
  const newNumber = getComplexNumberFromPaper(paper, xInCanvas, yInCanvas);

  if (lastNumber != undefined && lastNumber.re != undefined && lastNumber.im != undefined) {
    return perturbZeroes(newNumber, lastNumber, drawLogicSettings.perturbZeroesOnOriginalCanvasEpsilon);
  } else {
    return newNumber
  }
}

function getComplexNumberFromPaper(paper, xInCanvas, yInCanvas) {
  const xPixels = xInCanvas - paper.center.x;
  const yPixels = (yInCanvas - paper.center.y) * (-1);
  xCoordinates = xPixels / paper.unitLength
  yCoordinates = yPixels / paper.unitLength
  return math.complex({ re: xCoordinates, im: yCoordinates });
}

function handleDrawOriginal(from, to, paperIndex) {
  const lineWidths = [styleSettings.drawWidth].flat();
  const lineColors = [styleSettings.drawColor].flat();
  papers[paperIndex].pen.lineWidth = lineWidths[realMod(0 + cumulativeStyleShiftInside, lineWidths.length)];
  papers[paperIndex].pen.strokeStyle = lineColors[realMod(0 + cumulativeStyleShiftInside, lineColors.length)];
  paintLineBetweenComplexNumbersOnPaper(from, to, paperIndex);
}

function handleTransformerFunctionsOfComplexNumber(number, transformerFunctions) {
  return transformerFunctions.map(entry => {
    const tmpResults = [entry.transformer.func(number)].flat(); // put into array and flatten to make single numbers into an array with a single number and leave arrays as they are
    const resultsAdjusted = [
      ...tmpResults,
      ...(entry.transformer.expectedNumberOfResults == undefined ? [] : copies(tmpResults[tmpResults.length - 1], entry.transformer.expectedNumberOfResults - tmpResults.length))
    ]
    
    const amount = resultsAdjusted.length;
    const resultsWithStyling = resultsAdjusted.map((result, index) => ({
      number: result,
      lineColor: entry.lineColors[realMod(index - cumulativeIndexShifts, amount) % entry.lineColors.length],
      lineWidth: entry.lineWidths[realMod(index - cumulativeIndexShifts, amount) % entry.lineWidths.length],
    }));
    return resultsWithStyling;
  }).flat();
}

function handleDrawTransformedEntries({
  lastTransformedEntries, newTransformedEntries, paperIndex,
  shiftStyleIndex, // optional
  interrupted, // optional
}) {
  const amount = newTransformedEntries.length;
  // for (let index = 0; index < amount; index++) {
  for (let index = 0; index < amount; index++) {
    const adjustedStyleIndex = (
      shiftStyleIndex == undefined ?
      index :
      realMod(index + shiftStyleIndex, amount)
    );
    const lineWidths = [newTransformedEntries[adjustedStyleIndex].lineWidth].flat();
    const lineColors = [newTransformedEntries[adjustedStyleIndex].lineColor].flat();
    papers[paperIndex].pen.lineWidth = lineWidths[realMod(0 + cumulativeStyleShiftInside, lineWidths.length)];
    papers[paperIndex].pen.strokeStyle = lineColors[realMod(0 + cumulativeStyleShiftInside, lineColors.length)];

    const lastNumber = lastTransformedEntries[index].number;
    const newNumber = newTransformedEntries[index].number;
    paintLineBetweenComplexNumbersOnPaper(
      (interrupted ? newNumber : lastNumber),
      newNumber,
      paperIndex
    )
  }
}

let cumulativeStyleShiftInside = 0;
function checkBirkhoffCrossing(lastNumber, newNumber) {
  if (
    (
      math.abs(lastNumber.re) < 1 || math.abs(newNumber.re) < 1
    ) && (
      lastNumber.im == 0 ||
      newNumber.im == 0 ||
      lastNumber.im * newNumber.im < 0
    )
  ) {
    if (drawLogicSettings.birkhoffStyleSwitch) {
      cumulativeStyleShiftInside += 1;
    }
    return true;
  } else {
    return false;
  }
}

let cumulativeIndexShifts = 0;
function handleDrawTransformedEntriesCorrectedEntryOrder(lastTransformedEntries, newTransformedEntries, paperIndex) {
  // check which entries should go together by checking which one is the closest. This only works for sufficiently small mouse movements. Do this by only checking which of the new entries is closest to the 0-index-entry of the old entries and shifting cyclically.
  let closestEntryIndexInNew = undefined;
  let closestDistance = undefined;
  for (let i = 0; i < newTransformedEntries.length; i++) {
    const distance = distanceComplex(lastTransformedEntries[0].number, newTransformedEntries[i].number);
    if (
      (closestEntryIndexInNew == undefined && closestDistance == undefined) ||
      distance < closestDistance
    ) {
      if (closestEntryIndexInNew != undefined) console.log("closestDistance before", closestDistance)

      closestEntryIndexInNew = i;
      closestDistance = distance;

      // if (closestEntryIndexInNew != 0) {
      //   console.log("lastTransformedEntries", lastTransformedEntries)
      //   console.log("newTransformedEntries", newTransformedEntries)
      //   console.log("distance last0 new0", distanceComplex(lastTransformedEntries[0].number, newTransformedEntries[0].number))
      //   console.log("distance last0 new1", distanceComplex(lastTransformedEntries[0].number, newTransformedEntries[1].number))
      //   console.log("distance last0 new2", distanceComplex(lastTransformedEntries[0].number, newTransformedEntries[2].number))
      //   console.log("closestEntryIndexInNew", closestEntryIndexInNew)
      //   console.log("closestDistance after", closestDistance)
      // }
    }
  }

  if (drawLogicSettings.drawingMode == DRAWING_MODE.CONNECTED_CLOSEST_WITH_COLORS) {
    cumulativeIndexShifts += closestEntryIndexInNew; // CONNECTED_CLOSEST_WITH_COLORS
  }

  const newTransformedEntriesAdjusted = (
    closestEntryIndexInNew == 0 ?
    newTransformedEntries :
    shiftArrayEntries(newTransformedEntries, -closestEntryIndexInNew)
  );

  handleDrawTransformedEntries({
    lastTransformedEntries,
    newTransformedEntries: newTransformedEntriesAdjusted,
    paperIndex,
    shiftStyleIndex: (drawLogicSettings.drawingMode == DRAWING_MODE.CONNECTED_CLOSEST_WITH_COLORS ? (closestEntryIndexInNew) : undefined)
  });
}


function paintLineBetweenComplexNumbersOnPaper(fromNumber, toNumber, paperIndex) {
  const paper = papers[paperIndex]
  const pen = paper.pen;

  // maybe set this somewhere else, and maybe just once when the background is done drawing
  pen.lineCap = 'round';
  
  drawLine(
    pen,
    getCanvasPositionFromComplexNumber(paper, fromNumber),
    getCanvasPositionFromComplexNumber(paper, toNumber)
  );
}

function getCanvasPositionFromComplexNumber(paper, complex) {
  return {
    x: paper.center.x + (complex.re * paper.unitLength),
    y: paper.center.y - (complex.im * paper.unitLength),
  }
}



// run the initial stuff
window.addEventListener('load', ()=>{
  // styling
  body.style.backgroundColor = styleSettings.backgroundColor;
  body.style.color = styleSettings.textColor;

  // canvas
  updateDimensions();
  drawBackground();

  // // mouse
  // papers[0].canvas.addEventListener('mousedown', handleCanvasMouseDown);
  // document.addEventListener('mouseup', handleMouseUp);
  // document.addEventListener('mousemove', paint);

  // pointer, this is a generalization of mouse events
  papers[0].canvas.addEventListener('pointerdown', handleCanvasMouseDown);
  document.addEventListener('pointerup', handleMouseUp);
  document.addEventListener('pointermove', paint);

  // window resize
  window.addEventListener('resize', resize);

  // resizing of papers-container
  let observer = new ResizeObserver(function(mutations) {
    // console.log('mutations:', mutations);
    resizedPapersContainer = true;
  });
  observer.observe(papersContainer);
});














/// helpers

function clamp(num, min, max) {
  return Math.min(Math.max(num, min), max);
}
function range(start, end, step) { // start inclusive, end exclusive
  const resultArray = [];
  const stepSafe = (step == undefined ? 1 : step);
  for (let i = start; i < end; i = i+stepSafe) {
    resultArray.push(i);
  }
  return resultArray;
}
function copies(element, amount) {
  const resultArray = [];
  for (let i = 0; i < amount; i++) {
    resultArray.push(element);
  }
  return resultArray;
}
function shiftArrayEntries(array, shiftDistance) { // shiftDistance can be negative
  return range(0, array.length).map(index =>
    array[realMod(index - shiftDistance, array.length)]
  )
}
function distanceComplex(first, second) {
  return math.abs(math.subtract(first, second));
}

function realMod(number, modulus) {
  return ((number % modulus) + modulus) % modulus;
}
function bounceMod(number, modulus) { // returns a bouncing modulus, i.e for modulus 1 this returns 0, 0.2, 0.4, 0.6, 0.8, 1.0, 0.8, 0.6, 0.4, 0.2, 0, 0.2, ...
  const mod = realMod(number, 2 * modulus);
  return (
    mod > modulus ?
    2 * modulus - mod :
    mod
  )
}
function perturbZeroes(numberToPerturb, numberGravitator, epsilon) {
  if (epsilon == 0) {
    return numberToPerturb;
  }

  if (numberToPerturb.re == 0) {
    numberToPerturb.re += (numberGravitator.re < 0 ? -1 : 1) * epsilon;
  }
  if (numberToPerturb.im == 0) {
    numberToPerturb.im += (numberGravitator.im < 0 ? -1 : 1) * epsilon;
  }
  return numberToPerturb;
}
function spread(fromComplex, toComplex, maxDistance, perturbZeroesEpsilon) { // returns an array containing complexNumbers that lie between fromComplex and toComplex, with max distance of maxDistance between them, usually with the last two entries closer together, since fromComplex and toComplex will always be included
  const between = math.subtract(toComplex, fromComplex);
  const distance = math.abs(between);
  if (distance < maxDistance) {
    return [fromComplex, toComplex];
  }

  const amountLimit = 777; // this is an arbitrary limit to avoid careless settings to crash the browser
  const fittingAmount = Math.floor(distance / maxDistance);
  if (fittingAmount == 0) { // this should not be possible after the above if-check, but just in case, to avoid dividng by zero, in case there are rounding errors
    return [fromComplex, toComplex];
  }

  let distanceIterator;
  if (fittingAmount > amountLimit) {
    console.warn(`Arbitrary processing limiter of ${amountLimit} times kicked in and increased your maxDistance between drawn and calculated points.`)
    distanceIterator = math.divide(between, amountLimit);
  } else {
    distanceIterator = math.multiply(between, (maxDistance / math.abs(between)));
  }

  const returnArray = [];
  returnArray.push(fromComplex)

  if (perturbZeroesEpsilon != undefined && perturbZeroesEpsilon != 0) {
    for (let i = 1; i < Math.min(fittingAmount, amountLimit); i++) {
      const nextNumber = perturbZeroes(
        math.add(fromComplex, math.multiply(distanceIterator, i)),
        returnArray[i - 1],
        perturbZeroesEpsilon
      );
      returnArray.push(nextNumber);
    }
  } else {
    for (let i = 1; i < Math.min(fittingAmount, amountLimit); i++) {
      const nextNumber = math.add(fromComplex, math.multiply(distanceIterator, i));
      returnArray.push(nextNumber)
    }
  }
  
  returnArray.push(toComplex)

  return returnArray;
}

function isArrayOfType(array, type) {
  return (
    Array.isArray(array)
    && array.every(el => (typeof el) === type)
  )
}

function getArrayNextEntry(array, current) {
  const currentIndex = array.indexOf(current);
  const index = (currentIndex === -1) ? 0 : currentIndex;
  const next = array[(index + 1) % array.length];
  return next;
}

function degreeToRadians(degree) {
  return 2 * Math.PI * (degree / 360)
}

function drawLine(pen, from, to) {
  pen.beginPath();
  pen.moveTo(from.x, from.y);
  pen.lineTo(to.x, to.y);
  pen.stroke();
}
function drawLineWithFromTo(pen, lineWithFromTo) {
  pen.beginPath();
  pen.moveTo(lineWithFromTo.from.x, lineWithFromTo.from.y);
  pen.lineTo(lineWithFromTo.to.x, lineWithFromTo.to.y);
  pen.stroke();
}
function drawLineHorizontal(pen, y) {
  pen.beginPath();
  pen.moveTo(0, y);
  pen.lineTo(pen.canvas.width, y);
  pen.stroke();
}
function drawLineVertical(pen, x) {
  pen.beginPath();
  pen.moveTo(x, 0);
  pen.lineTo(x, pen.canvas.height);
  pen.stroke();
}

function drawArc(pen, center, radius, startRadians, endRadians, filled) {
  pen.beginPath();
  pen.arc(center.x, center.y, radius, -endRadians, -startRadians);
  if (filled) {
    pen.fill()
  }
  pen.stroke();
}

function drawPolygon(pen, points, stroke, filled) {
  if (points === undefined) {
    console.warn("points undefined in drawPolygon");
    return;
  }

  const amount = points.length;

  pen.beginPath();
  pen.moveTo(points[0].x, points[0].y);
  for (let i = 1; i < amount; i++) {
    pen.lineTo(points[i].x, points[i].y);
  }
  pen.closePath();

  if (stroke) {
    pen.stroke();
  }

  if (filled) {
    pen.fill()
  }
}


function drawTextAtPositions(pen, positions, yOffset, text, fill, stroke) {
  positions.map(position => {
    drawTextAtPosition(pen, position, yOffset, text, fill, stroke);
  });
}

function drawTextAtPosition(pen, position, yOffset, text, fill, stroke) {
  if (fill) {
    pen.fillText(text, position.x, position.y + yOffset); // need to offset the y-coordinate by yOffset or else the text will be too high
  }
  if (stroke) {
    pen.fillText(text, position.x, position.y + yOffset);
  }
}



// TODOs
// line drawing mode
// add settings UI and make all things toggleable
// upload image that is then displayed in background of drawing canvas, centered max dimensions, maybe with new native html modal (toggle with button in lower left?) that can resize and move the image and change transparency. With Drag&Drop would be nice.
// bifurcation mode: redraw the curve so far (press B to keep drawing old points) displaced by current mouse position from origin, with some smoothing? Maybe also show where on the old curve we are redrawing at the moment.
// save curves in localStorage -> in the settings menu (with info-popup saying that it is not saved online, but on the device). Save all the points drawn, so it can be redrawn with in-engine transformations, maybe a different one than originally.
// undo feature?


